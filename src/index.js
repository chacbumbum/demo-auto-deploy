/* eslint-disable no-console */
const logger = require('winston');
const config = require('config');

const App = require('./app');

const AppInstance = new App(config);
AppInstance.start().then(app => {
    const server = app.listen(config.port);

    server.on('listening', () => logger.info(`Feathers application started on ${app.get('host')}:${app.get('port')}`));
});

process.on('unhandledRejection', (reason, p) => logger.error('Unhandled Rejection at: Promise ', p, reason));
