// Initializes the `country` service on path `/country`
const createService = require('./country.class.js');
const hooks = require('./country.hooks');
const filters = require('./country.filters');

module.exports = function() {
    const app = this;
    const paginate = app.get('paginate');

    const options = {
        name: 'country',
        paginate
    };

    // Initialize our service with any options it requires
    app.use('/country', createService(options));

    // Get our initialized service so that we can register hooks and filters
    const service = app.service('country');

    service.hooks(hooks);

    if (service.filter) {
        service.filter(filters);
    }
};
