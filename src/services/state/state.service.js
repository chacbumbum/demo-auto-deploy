// Initializes the `state` service on path `/state`
const createService = require('./state.class.js');
const hooks = require('./state.hooks');
const filters = require('./state.filters');

module.exports = function() {
    const app = this;
    const paginate = app.get('paginate');

    const options = {
        name: 'state',
        paginate
    };

    // Initialize our service with any options it requires
    app.use('/state', createService(options));

    // Get our initialized service so that we can register hooks and filters
    const service = app.service('state');

    service.hooks(hooks);

    if (service.filter) {
        service.filter(filters);
    }
};
