/* eslint-disable no-unused-vars */

const mongoose = require('mongoose');

const Cinema = mongoose.model('Cinema');

class Service {
    constructor(options) {
        this.options = options || {};
    }

    find(params) {
        const { cityId } = params.query;
        return Cinema.getCinemasByCity(cityId);
    }

    get(id, params) {
        return Cinema.getCinemaById(id);
    }

    create(data, params) {
        if (Array.isArray(data)) {
            return Promise.all(data.map(current => this.create(current)));
        }

        return Promise.resolve(data);
    }

    update(id, data, params) {
        return Promise.resolve(data);
    }

    patch(id, data, params) {
        return Promise.resolve(data);
    }

    remove(id, params) {
        return Promise.resolve({ id });
    }

    getSchedule(params) {
        const { cityId, movieId } = params;
        const par = {
            cityId,
            movieId
        };
        return Cinema.getCinemaScheduleByMovie(par);
    }
}

module.exports = function(options) {
    return new Service(options);
};

module.exports.Service = Service;
