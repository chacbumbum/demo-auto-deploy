// Initializes the `cinema` service on path `/cinema`
const createService = require('./cinema.class.js');
const hooks = require('./cinema.hooks');
const filters = require('./cinema.filters');

module.exports = function() {
    const app = this;
    const paginate = app.get('paginate');

    const options = {
        name: 'cinema',
        paginate
    };

    // Initialize our service with any options it requires
    app.use('/cinema', createService(options));

    app.use('/cinema/:cityId/:movieId', {
        find: createService(options).getSchedule
    });
    // app.get('/cinema/:cityId/:movieId', createService(options).schedule);

    // Get our initialized service so that we can register hooks and filters
    const service = app.service('cinema');

    service.hooks(hooks);

    if (service.filter) {
        service.filter(filters);
    }
};
