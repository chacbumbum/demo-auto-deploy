const country = require('./country/country.service.js');
const city = require('./city/city.service.js');
const state = require('./state/state.service.js');
const cinema = require('./cinema/cinema.service.js');
module.exports = function() {
    const app = this; // eslint-disable-line no-unused-vars
    app.configure(country);
    app.configure(city);
    app.configure(state);
    app.configure(cinema);
};
