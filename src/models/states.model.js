// states-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
const mongoose = require('mongoose');

const StateSchema = new mongoose.Schema(
    {
        name: { type: String, required: true },
        country_id: { type: String, required: true },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now }
    },
    {
        timestamps: true,
        collection: 'states'
    }
);

mongoose.model('State', StateSchema);
