// cities-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
const mongoose = require('mongoose');
const CitySchema = new mongoose.Schema(
    {
        name: { type: String, required: true },
        state_id: { type: String, required: true },
        cinemas: [{ type: String }]
    },
    {
        timestamps: true,
        collection: 'cities'
    }
);

mongoose.model('City', CitySchema);
