// cinemas-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.

const mongoose = require('mongoose');

const CinemaSchema = new mongoose.Schema(
    {
        name: { type: String, required: true },
        city_id: { type: String, required: true },
        cinemaPremieres: [],
        cinemaRooms: []
    },
    {
        timestamps: true,
        collection: 'cinemas'
    }
);

class CinemaLogic {
    static getCinemasByCity(cityId) {
        return this.find({
            city_id: cityId
        });
    }

    static getCinemaById(id) {
        return this.findById(id);
    }

    static getCinemaScheduleByMovie(options) {
        const match = {
            $match: {
                city_id: options.cityId,
                'cinemaRooms.schedules.movie_id': options.movieId
            }
        };

        const project = {
            $project: {
                name: 1,
                'cinemaRooms.schedules.time': 1,
                'cinemaRooms.name': 1,
                'cinemaRooms.format': 1
            }
        };

        const unwind = [{ $unwind: '$cinemaRooms' }, { $unwind: '$cinemaRooms.schedules' }];
        const group = [
            {
                $group: {
                    _id: {
                        name: '$name',
                        room: '$cinemaRooms.name'
                    },
                    schedules: { $addToSet: '$cinemaRooms.schedules.time' }
                }
            },
            {
                $group: {
                    _id: '$_id.name',
                    schedules: {
                        $addToSet: {
                            room: '$_id.room',
                            schedules: '$schedules'
                        }
                    }
                }
            }
        ];

        return this.aggregate([match, project, ...unwind, ...group]);
    }
}

CinemaSchema.loadClass(CinemaLogic);

mongoose.model('Cinema', CinemaSchema);
// return mongooseClient.model('cinemas', cinemas);
