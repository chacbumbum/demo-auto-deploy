const mongoose = require('mongoose');
const fs = require('fs');

class MongoService {
    constructor(_config) {
        this.config = _config;
    }
    loadModel() {
        const modelPath = __dirname + '/models';
        const files = fs.readdirSync(modelPath);
        files.forEach(file => {
            require(`${modelPath}/${file}`);
        });
    }
    connect() {
        mongoose.Promise = global.Promise;
        return mongoose.connect(this.config.mongodb).then(() => {
            this.loadModel();
            return mongoose;
        });
    }
}

module.exports = MongoService;
