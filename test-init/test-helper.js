/**
 * Vault Dragon Patient App
 * API Server
 * Test Helper
 * Copyright 2016
 */
'use strict';

const { EventEmitter } = require('events');
const _ = require('lodash');
const readyEventEmitter = new EventEmitter();

/**
 * Stuff to help detect when application ready. Also define some functions to write tests case
 * This class is singleton
 *
 * @class TestHelper
 */
class TestHelper {
    constructor() {
        this.ready = false;
    }

    /**
     * Register onReady event callback
     *
     * @param {any} callback
     * @returns
     */
    onReady(callback) {
        if (this.ready && this.ready === true) {
            return callback();
        }

        readyEventEmitter.on('ready', function() {
            callback();
        });
    }

    /**
     * Set ready state of test client
     */
    setReady() {
        this.ready = true;
        readyEventEmitter.emit('ready');
    }
}

module.exports = exports = new TestHelper();
