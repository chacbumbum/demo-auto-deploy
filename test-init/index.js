const logger = require('winston');
const config = require('config');

global.testHelper = require('./test-helper');

const App = require('./../src/app');

const AppInstance = new App(config);
AppInstance.start()
    .then(app => {
        global.app = app;
        const server = app.listen(config.port);
        server.on('listening', () =>
            logger.info(`Feathers application started on ${app.get('host')}:${app.get('port')}`)
        );
        testHelper.setReady();
    })
    .catch(() => {
        process.exit(1);
    });
